# Скрипт для перевода полей в author_info

Связан с задачей http://youtrack.umisoft.ru/issue/umi-2838

* Склонировать репозиторй в корне контроллера в своей папке в /scripts
* Запустить `php update/run.php --dry-run --script=translate_author_info.php` для тестового прогона (посмотреть что получится).
* Запустить `php update/run.php --script=translate_author_info.php` для реального выполнения действий.