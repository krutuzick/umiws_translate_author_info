<?php
/**
 * Скрипт который будет идти по каждому сайту по всему списку полей. 
 * Если в поле что-то заполенено и есть русские буквы - то вставить стандартное испанское наполенение, иначе - ничего не делать.
 * Таким образом уже переведённые (особенные для дизайна) поля не изменятся, а скрытые или невидимые - переведутся.
 */

$dry_run = isset($options['dry-run']);

define('TOKEN_EMPTY_STRING', '{{{[[[INSCRIPT EMPTY STRING]]]}}}');

$map = array(
	// Группа "Авторская информация"
	'kod_goroda' => '+34', //Код города
	'telefon' => '657567567', //Телефон
	'rezhim_raboty' => 'Horario: 24 horas', //Режим работы
	'imya_i_familiya' => 'Pedro Rodriquez', //Имя и фамилия
	'professiya_ili_slogan' => 'Aquí aparece eslogan', //Профессия или слоган
	'mainpage_news_title' => 'Noticias', //Заголовок блока новостей на главной
	'mainpage_albums_title' => 'Galería', //Заголовок блока альбомов на главной
	'mainpage_catalog_title' => 'Servicios', //Заголовок блока каталога на главной
	'mainpage_blogs_title' => 'Blog', //Заголовок блока блогов на главной
	'shortbasket_title' => 'Cesta', //Название корзины в компактном блоке
	'emptybasket_title' => 'Cesta vacía', //Название пустой корзины в компактном блоке
	'basket_title' => 'Cesta de la compra', //Заголовок корзины
	'order_title' => 'Tramitar pedido', //Заголовок страницы с результатом заказа
	'order_success_text' => '¡Su pedido fue realizado con éxito! Por favor, espera respuesta de nuestro manager. Si no nos ponemos en contacto con usted en 20 minutos durante el tiempo laboral, por favor llámenos.', //Текст успешного заказа
	'order_failed_text' => 'Ocurrió un error al crear el pedido. Intente de nuevo.', //Текст ошибки при заказе
	'buy_title' => 'Comprar', //Текст ссылки КУПИТЬ
	'feedback_title' => 'Formulario de contacto', //Заголовок формы обратной связи
	'good_descr' => 'Descripción', //Заголовок описания товара
	'catalog_customer_title' => TOKEN_EMPTY_STRING, //Заголовок производителя
	'catalog_price_title' => 'Precio', //Заголовок цены
	'currency' => 'euro', //Сокращённое название валюты
	'contacts_address_title' => 'Dirección:', //Заголовок адреса на странице контактов
	'contacts_phones_title' => 'Teléfono:', //Заголовок телефонов на странице контактов
	'comments_title' => 'Comentarios', //Заголовок блока комментариев
	'add_comment_title' => 'Añadir comentario:', //Заголовок блока добавления комментария
	'add_comment_success_text' => 'Su mensaje ha sido agregado con éxito, pero de acuerdo con la política de la página web estará disponible al público después de la moderación por el administrador.', //Текст сообщения о добавлении комментария
	'show_all_photo_title' => 'Mostrar todo', //Текст ссылки ПОКАЗАТЬ ВСЕ ФОТОГРАФИИ
	'page404_text' => 'La página solicitada no se ha encontrado', //Текст не найденной страницы
	'custom_field_title' => TOKEN_EMPTY_STRING, //Произвольное поле 1
	'custom_field2_title' => TOKEN_EMPTY_STRING, //Произвольное поле 2
	'comments_do_title' => 'Comentar', //Текст ссылки КОММЕНТИРОВАТЬ
	'nocomments_title' => 'Sin comentarios', //Текст ссылки КОММЕНТАРИЕВ НЕТ
	'catalog_import_default_name' => 'Servicios', //Название каталога при импорте по умолчанию
	'title_order_link' => 'Tramitar', //Текст ссылки ЗАКАЗАТЬ
	'copyright' => TOKEN_EMPTY_STRING, //Текст футера
	'mainpage_news_show_all' => 'Mostrar todo', //Показать все новости
	'mainpage_catalog_show_all' => 'Mostrar todo', //Показать все товары
	'mainpage_albums_show_all' => 'Mostrar todo', //Показать все альбомы
	'address' => 'C/ Recogidas 25, Granada', //Dirección
	//'breadcrumbs_index' => 'Home', //Главная
	'breadcrumbs_index' => 'Inicio', //Главная
	'header_basket_goods' => 'Artículos:', //Artículos
	'header_basket_sum' => 'Monto:', //Сумма
	'code_head' => TOKEN_EMPTY_STRING, //Код в теге head
	'code_footer' => TOKEN_EMPTY_STRING, //Код в футере сайта
	'custom_block_1' => TOKEN_EMPTY_STRING, //Настраиваемый HTML блок 1
	'custom_block_2' => TOKEN_EMPTY_STRING, //Настраиваемый HTML блок 2
	'empty_basket_text' => '<p>El carrito de compra está vacio.</p>', //Текст на странице пустой корзины
	'banner_title_1' => TOKEN_EMPTY_STRING, //Заголовок баннера 1
	'banner_title_2' => TOKEN_EMPTY_STRING, //Заголовок баннера 2
	'banner_title_3' => TOKEN_EMPTY_STRING, //Заголовок баннера 3
	'banner_link_1' => TOKEN_EMPTY_STRING, //Ссылка для баннера 1
	'banner_link_2' => TOKEN_EMPTY_STRING, //Ссылка для баннера 2
	'banner_link_3' => TOKEN_EMPTY_STRING, //Ссылка для баннера 3
	//'reviews_title' => 'Comentarios', //Заголовок блока отзывов
	'reviews_title' => 'Opiniones', //Заголовок блока отзывов
	//'add_reviews_title' => 'Adicionar comentario', //Заголовок блока добавления отзыва
	'add_reviews_title' => 'Añadir opinión', //Заголовок блока добавления отзыва
	'copyright_umicms' => TOKEN_EMPTY_STRING, //Текст футера umi.cms
	'catalog_old_price_title' => 'Otro precio', //Заголовок старой цены
	'banner_html_1' => TOKEN_EMPTY_STRING, //HTML для баннера 1
	'banner_html_2' => TOKEN_EMPTY_STRING, //HTML для баннера 2
	'banner_html_3' => TOKEN_EMPTY_STRING, //HTML для баннера 3
	'mainpage_special_offers_title' => 'Oferta especial', //Заголовок блока специальных предложений на главной
	'comments_widget_title' => 'Comentarios sobre la página web', //Заголовок виджета отзывов
	'comments_page_link_title' => 'Todos los comentarios', //Текст ссылки на страницу отзывов
	'id_vkontakte' => TOKEN_EMPTY_STRING, //ID сообщества вконтакте
	'mainpage_popular_offers_title' => 'Artículos populares', //Заголовок блока популярных товаров на главной
	'delivery_option_select_title' => 'Seleccione el método de envío:', //Текст в поле рядом с выбором вариант доставки
	'title_for_contacts' => 'Contactos', //Заголовок блока контактов на главной
	'title_vk' => 'VK', //Заголовок блока ВКонтакте
	'basket_text' => TOKEN_EMPTY_STRING, //Текст на странице корзины
	//'add_compare_button_text' => 'Añadir a al comparación', //Título del botón para añadir artículos a la comparación
	'add_compare_button_text' => 'Añadir a la comparación', //Título del botón para añadir artículos a la comparación
	'goto_compare_button_text' => 'Ir a la comparación', //Título del botón de la comparación de artículos
	'field_extra_goods_caption' => 'Artículos relacionados', //Título de los artículos relacionados
	
	
	// Группа "Форма заказа"
	'order_form_title' => 'Formulario de contacto', //Текст заголовка формы
	'order_phone_title' => 'Teléfono:', //Заголовок для телефона
	'order_form_success' => 'Gracias por su interes. Nos pondremos en contacto con usted lo más pronto posible.', //Сообщение успешно отправлено
	'order_name_title' => 'Su nombre:', //Заголовок для имени
	'order_message_title' => 'Mensaje:', //Заголовок для заказа
);

$sel = new selector('objects');
$sel->types('object-type')->guid('umiruguid-type-author-info');
/** @var $author_info_object umiObject */
$author_info_object = $sel->first;

if ($author_info_object instanceof umiObject == false) {
	echo "  [ERROR] Failed to get author info object" . PHP_EOL;
	return;
}

umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;

foreach($map as $field_name => $default_value) {
	$current_value = $author_info_object->getValue($field_name);
	$new_value = ($default_value == TOKEN_EMPTY_STRING) ? "" : $default_value;
	if ($dry_run) {
		echo "  [INFO] Field [{$field_name}] got value [{$current_value}]" . PHP_EOL;
	}
	if (valueInvalid($field_name, $current_value)) {
		if ($dry_run) {
			echo "    [DRY-INFO] change value to [{$new_value}]" . PHP_EOL;
		} else {
			$author_info_object->setValue($field_name, $new_value);
			echo "  [INFO] Field [{$field_name}] got value [{$current_value}] changed to [{$new_value}]" . PHP_EOL;
		}
	}
}

if (!$dry_run) {
	$author_info_object->commit();
}

/**
 * Проверка - нужно ли в поле подставлять значение "по умолчанию"
 * @param string $field_name Системное название поля
 * @param string $field_value Текущее значение поля в странице "Для вставки"
 * @return bool
 */
function valueInvalid($field_name, $field_value) {
	switch (true) {
		// Код города телефона - заменяем
		case $field_name == 'kod_goroda': return true;
		// Номер телефона - заменяем
		case $field_name == 'telefon': return true;
		// Багфикс корзины
		case $field_name == 'empty_basket_text' && $field_value == '&lt;p&gt;El carrito de compra está vacio.&lt;/p&gt;': return true;
		// Ссылка "Главная" в хлебных крошках
		case $field_name == 'breadcrumbs_index' && trim($field_value) == 'Home': return true;
		// Заголовок блока "Отзывы"
		case $field_name == 'reviews_title' && in_array(trim(mb_strtolower($field_value)), array('comentarios', 'comentarios:')): return true;
		// Заголовок "Добавить отзыв"
		case $field_name == 'add_reviews_title' && in_array(trim(mb_strtolower($field_value)), array('adicionar comentario', 'adicionar comentario:', 'adicionar comentarió', 'adicionar comentarió:', 'añadir comentario', 'añadir comentario:', 'añadir comentarió', 'añadir comentarió:')): return true;
		// Текст ссылки "Добавить в сравнение"
		case $field_name == 'add_compare_button_text' && trim($field_value) == 'Añadir a al comparación': return true;
		// Если поле непустое и содержит русские символы - то заменяем
		case $field_value != '' && preg_match('/[а-яА-ЯёЁ]/iu', $field_value): return true;
		// По-умолчанию - не заменяем
		default: return false;
	}
}